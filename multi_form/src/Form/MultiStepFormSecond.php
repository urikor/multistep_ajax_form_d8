<?php
/**
 * @file
 * Contains Drupal\multi_form\Form\MultiStepForm.
 */

namespace Drupal\multi_form\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class MultiStepFormSecond extends FormBase {

  private $first_step_fields_number;
  private $second_step_fields_number;

  public function __construct() {
    $this->first_step_fields_number = MultiStepFormSecond::getTableField(
      'config_pages__field_fields_number_first_step',
      'field_fields_number_first_step_value'
    );
    $this->second_step_fields_number = MultiStepFormSecond::getTableField(
      'config_pages__field_fields_number_second_step',
      'field_fields_number_second_step_value'
    );
  }

  public function getFormId() {
    return 'multi_step_admin_form_second';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $number = $this->second_step_fields_number;

    $form['#prefix'] = '<div id="multi-form">';
    $form['#suffix'] = '</div>';

    $form['multi_form']['second_step'] = array(
      '#type'  => 'fieldset',
      '#title' => t('Form second step'),
      '#prefix' => '<div id="multi-form">',
      '#suffix' => '</div>'
    );

    for ($i = 1; $i <= $number; $i++) {
      $form['multi_form']['second_step']['text_field_2_' . $i] = array(
        '#type'        => 'textfield',
        '#title'       => t('Text field second' . $i),
        '#maxlength'   => 60,
        '#placeholder' => t('Enter string'),
      );
    }

    $form['multi_form']['next'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
      '#ajax' => array(
        'callback' => array($this, 'sendMail'),
        'wrapper' => 'multi-form',
        'event' => 'click',
        'progress' => array(
          'type' => 'throbber',
          'message' => NULL,
        ),
      ),
    );

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  public function sendMail(array $form, FormStateInterface $form_state) {
    $form = \Drupal::formBuilder()->getForm('Drupal\multi_form\Form\MultiStepFormSecond');

    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new ReplaceCommand('#edit-first-step', '123'));

    return '123';
  }

  private static function getTableField($table, $field)
  {
    $out = FALSE;

    if ($table && $field) {
      $query  = \Drupal::database()->select($table, 't')
                       ->fields('t', array($field))
                       ->condition('bundle', 'test_form_settings');
      $result = $query->execute()->fetchField();

      if ($result) {
        $out = $result;
      }
    }

    return $out;
  }
}
