<?php
/**
 * @file
 * Contains Drupal\multi_form\Form\MultiStepForm.
 */

namespace Drupal\multi_form\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use \Drupal\Core\Link;

class MultiStepFormFirst extends FormBase {

  private $first_step_fields_number;
  private $second_step_fields_number;
  private $langcode;
  private $step = 1;

  public function __construct() {
    $first_step_fields_number = $this->getTableField(
      'config_pages__field_fields_number_first_step',
      'field_fields_number_first_step_value'
    );
    $this->first_step_fields_number = !empty($first_step_fields_number) ? $first_step_fields_number : 1;

    $second_step_fields_number = $this->getTableField(
      'config_pages__field_fields_number_second_step',
      'field_fields_number_second_step_value'
    );
    $this->second_step_fields_number = !empty($second_step_fields_number) ? $second_step_fields_number : 1;
  }

  public function getFormId() {
    return 'multi_step_admin_form_first';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->getFormByStep('first', $form);
    $this->getFormByStep('second', $form);

    $this->step = 1;

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  private function setStepFieldsNameArray($step_string, $form_state)
  {
    $fields = array();

    $number_field_name = $step_string . '_step_fields_number';
    $number = $form_state->getValue($number_field_name);
    for ($i = 1; $i <= $number; $i++) {
      $fields[] = 'text_field_' . $step_string . $i;
    }

    return $fields;
  }

  public function getFormByStep($step_string, &$form)
  {
    $style = '';
    if ($this->step === 1 && $step_string == 'second') {
      $style = 'display:none';
    }

//    $form['links'] = $this->getLinksList();

    $form['multi_form'][$step_string . '_step'] = array(
      '#type'  => 'fieldset',
      '#title' => t('Form @step step', array('@step' => $step_string)),
      '#attributes' => array(
        'style' => $style,
        'class' => array($step_string),
      ),
    );

    $step_field = $step_string . '_step_fields_number';
    $number = $this->$step_field;
    for ($i = 1; $i <= $number; $i++) {
      $form['multi_form'][$step_string . '_step']['text_field_' . $step_string . $i] = array(
        '#type'        => 'textfield',
        '#title'       => t('Text field first' . $i),
        '#name'        => 'text_field_' . $step_string . $i,
        '#maxlength'   => 60,
        '#placeholder' => t('Enter string'),
      );
    }

    if ($step_string == 'first') {
      $form['actions']['next'] = array(
        '#type'   => 'button',
        '#value'  => t('Next'),
        '#weight' => 1,
        '#attributes' => array(
          'class' => array($step_string),
        ),
        '#ajax'   => array(
          'callback' => array($this, 'stepCallback'),
          'progress' => array(
            'type'    => 'throbber',
            'message' => NULL,
          ),
        ),
      );
      $form['first_step_fields_number'] = array(
        '#type' => 'hidden',
        '#value' => $this->first_step_fields_number,
      );
      $form['second_step_fields_number'] = array(
        '#type' => 'hidden',
        '#value' => $this->second_step_fields_number,
      );
    }

    if ($step_string == 'second') {
      $form['actions']['prev'] = array(
        '#type'   => 'button',
        '#value'  => t('Previous'),
        '#weight' => 0,
        '#attributes' => array(
          'style' => $style,
          'class' => array($step_string),
        ),
        '#ajax'   => array(
          'callback' => array($this, 'stepCallback'),
          'progress' => array(
            'type'    => 'throbber',
            'message' => NULL,
          ),
        ),
      );
      $form['actions']['send'] = array(
        '#type'   => 'submit',
        '#value'  => t('Send'),
        '#weight' => 2,
        '#attributes' => array(
          'style' => $style,
          'class' => array($step_string),
        ),
        '#ajax'   => array(
          'callback' => array($this, 'sendCallback'),
          'progress' => array(
            'type'    => 'throbber',
            'message' => NULL,
          ),
        ),
      );
    }
  }

  public function stepCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $class_to_hide = $class_to_show = '';
    $op = $form_state->getValue('op');
    if ($op && $op == 'Next') {
      $this->step = 2;
      $class_to_hide = '.first';
      $class_to_show = '.second';
    }

    if ($op && $op == 'Previous') {
      $this->step = 1;
      $class_to_hide = '.second';
      $class_to_show = '.first';
    }

    if ($class_to_hide && $class_to_show) {
      $response->addCommand(new InvokeCommand($class_to_hide, 'hide'));
      $response->addCommand(new InvokeCommand($class_to_show, 'fadeIn', array('duration' => 'fast')));
    }

    return $response;
  }

  public function sendCallback(array &$form, FormStateInterface $form_state) {
    $first_step_fields = $this->setStepFieldsNameArray('first', $form_state);
    $second_step_fields = $this->setStepFieldsNameArray('second', $form_state);

    $fields = array_merge($first_step_fields, $second_step_fields);

    if (!empty($fields)) {
      $values = $form_state->getValues();
      $body = '';
      foreach ($fields as $field) {
        $body .= !empty($values[$field]) ? $field . ': ' . $values[$field] . '<br />' : '';
      }

      if (!empty($body)) {
        $mailManager       = \Drupal::service('plugin.manager.mail');
        $module            = 'multi_form';
        $key               = 'multi_step_form_mail';

        $mail_to = $this->getTableField(
          'config_pages__field_mail_to',
          'field_mail_to_value'
        );
        $to = !empty($mail_to) ? $mail_to : 'example@mailinator.com';

        $subject = $this->getTableField(
          'config_pages__field_mail_subject',
          'field_mail_subject_value'
        );
        $langcode = \Drupal::currentUser()->getPreferredLangcode();
        $params['subject'] = !empty($subject) ? $subject :
          t('Multi Step Form fields from "@site"', array(
            'langcode' => $langcode,
            '@site'    => \Drupal::config('system.site')->get('name'),
          ));

        $params['from'] = \Drupal::config('system.site')->get('mail');
        $params['body'] = $body;
        $send           = TRUE;

        $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

        if ($result['result'] !== TRUE) {
          $message = t('There was a problem sending your message and it was not sent.');
        }
        else {
          $message = t('Your message has been sent.');
        }
      } else {
        $message = t('All fields empty, nothing to send.');
      }
    }

    $message = !empty($message) ? $message : '';
    $url = Url::fromRoute('multi_form.form_page');

    $links = '';
    $links .= '<ul>';
    $links .= '<li><a href="' . $url->toString() . '">' . t('Return') . '</a></li>';
    global $base_path;
    $links .= '<li><a href="' . $base_path . '">' . t('Home') . '</a></li>';
    $links .= '</ul>';

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#multi-step-admin-form-first', '<fieldset>' . $message . '</fieldset>' . $links));
    return $response;
  }

  private function getLinksList()
  {
    global $base_path;
    $url = Url::fromRoute('multi_form.form_page');

    return [
      '#theme' => 'multi_form_links',
      '#form' => $url->toString(),
      '#home' => $base_path,
      '#form_label' => t('Form'),
      '#home_label' => t('Home'),
    ];
  }

  private function getTableField($table, $field)
  {
    $out = FALSE;

    if ($table && $field) {
      $query  = \Drupal::database()->select($table, 't')
                       ->fields('t', array($field))
                       ->condition('bundle', 'test_form_settings');
      $result = $query->execute()->fetchField();

      if ($result) {
        $out = $result;
      }
    }

    return $out;
  }
}
